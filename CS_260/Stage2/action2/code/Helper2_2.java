 
import ij.IJ;
import ij.ImagePlus;
import ij.plugin.PlugIn;
import ij.process.ImageProcessor;
import ij.gui.NewImage;
import ij.plugin.ChannelSplitter;
import ij.plugin.RGBStackMerge;
import ij.process.ImageConverter;
import java.io.File;
import java.awt.Color;
import java.lang.Math;

public class Helper2_2 implements PlugIn {
    public void run(String Args) {
        /**
         * run method
         */
        String path = "/media/projects/CS_260"; // common path, in my case is the absolute path of the project
        String ds1 = "/FEI _Face_DS/cropped"; // path of the data set of images
        String ds2 = "/FEI _Face_DS"; // path of the data set of benchmark images
        String target = "/Stage2/action2"; // the specific path of the taskty
        String bCode = "-11";
        String[] codes = {"a", "b"}; // the second part of name of the image file
        for (int iname = 173; iname < 178; iname++) { // loop over all data set images //178
            for (String code:codes) {
                // the first part of name of the starting image file
                /**
                 * examine(String path, String ds1, String ds2, String target, String photo, String bPhoto)
                 */
                examine(path, ds1, ds2, target, "/"+iname+code, "/"+iname+bCode);
            }
        }
    }
    private int[] getCumulativeHistogram(ImageProcessor ip) {
        /**
         * get cumulative normalized histogram
         */
        int climax = 0;
        int[] cumulativeHist = new int[256];
        int[] hist = ip.getHistogram();

        for (int i = 0; i < 256; i++) { // cumulate
            for (int j = 0; j <= i; j++) {
                cumulativeHist[i] += hist[j];
            }
        }
        for (int count: cumulativeHist) { // find maximum
            if (count > climax) {
                climax = count;
            }
        }
        for (int i = 0; i < 256; i++) { // normalize
            cumulativeHist[i] = (1000*cumulativeHist[i]) / climax;
//            IJ.log("normal="+i+" = "+hist[i]);
        }
        return cumulativeHist;
    }
    private ImageProcessor embed (ImageProcessor sup, ImageProcessor sub, int pos) {
        /**
         * embed given sub images into sup image with regard of given position
         */
        int M = pos * sub.getWidth(), M0 = (pos - 1) * sub.getWidth();
        int N = sub.getHeight();
        for (int u = M0; u < M; u++) {
            for (int v = 0; v < N; v++) {
                if (sub.isGrayscale()) {
                    sup.putPixelValue(u, v, sub.getPixel(u - M0, v));
                } else {
                    sup.putPixel(u, v, sub.getPixel(u - M0, v));
                }
            }
        }
        return sup;
    }
    private ImageProcessor calibrate(ImageProcessor ip, ImageProcessor bip) {
        /**
         * calibrate the first argument with the second argument
         */
        int[] hist = new int[256];
        int[] bench = new int[256];
        int[] map = new int[256];
        int count, j;
        int width = ip.getWidth(), height = ip.getHeight();
        hist = getCumulativeHistogram(ip);
        bench = getCumulativeHistogram(bip);
        ImageProcessor answer = (ImageProcessor) ip.clone();
        for (int i = 0; i < 256; i++) {
            count = hist[i];
            j = match(count, bench);
            map[i] = j;
//            IJ.log("normal="+i+" = "+map[250]);
        }
        for (int u = 0; u < width; u++) {
            for (int v = 0; v < height; v++) {
                if (ip.isGrayscale()) {
                    answer.putPixelValue(u, v, map[ip.getPixel(u, v)]);
                } else {
                    answer.putPixel(u, v, map[ip.getPixel(u, v)]);
                }
            }
        }
        return answer;
    }
    int match(int count, int[] bench) {
        /**
         * search over given array and return the index of closest value to count
         */
        int[] difference = new int[256];
        for (int i = 0; i < 256; i++) {
            difference[i] = Math.abs(bench[i] - count);
        }
        int ans = 0;
        int ext = difference[0];
        for (int j = 0; j < 256; j++) {
            if (difference[j] < ext) {
                ext = difference[j];
                ans = j;
            }
        }
        return ans;
    }
    private ImageProcessor copy (ImageProcessor base, ImageProcessor clone) {
        /**
         * copy pixel by pixel base image to clone one
         */
        int M = base.getWidth();
        int N = base.getHeight();

        for (int u = 0; u < M; u++) {
            for (int v = 0; v < N; v++) {
                if (base.isGrayscale()) {
                    clone.putPixelValue(u, v, base.getPixel(u, v));
                } else {
                    clone.putPixel(u, v, base.getPixel(u, v));
                }
            }
        }
        return clone;
    }
    private void folderBuilder(String path, String target, String photo) {
        File file = new File(path+target+photo);
        boolean bool = file.mkdir();
        if (bool != false) IJ.log("photo folder is built.");
    }
    private void examine(String path, String ds1, String ds2, String target, String photo, String bPhoto) {
        String postfix = ".jpg";
        IJ.open(path+ds1+photo+postfix); // find the given photo name and open it
        IJ.log("main= "+path+ds1+photo+postfix);
        ImagePlus image = IJ.getImage(); // main image
        IJ.open(path+ds2+bPhoto+postfix); // find the given photo name and open it
        IJ.log("bench= "+path+ds2+bPhoto+postfix);
        ImagePlus bImage = IJ.getImage(); // benchmark image
        ImagePlus combination; // combination of all channels
        ImageConverter imc; // conver of all combination
        ImageProcessor combIp; // processor of all combination
        ImagePlus[] imageRGB = ChannelSplitter.split(image);
        ImagePlus[] bImageRGB = ChannelSplitter.split(bImage);

        ImagePlus [] clone = new ImagePlus[4]; // define four clones of the main photo
        ImageProcessor [] cip = new ImageProcessor[4];

        ImageProcessor mip = image.getProcessor(); // use to determine the dimensions
        int width = mip.getWidth();
        int height = mip.getHeight();

        clone[0] = NewImage.createRGBImage("temp" , width, height, 1, 1);
        clone[1] = NewImage.createRGBImage("temp" , width, height, 1, 1);
        clone[2] = NewImage.createRGBImage("temp" , width, height, 1, 1);
        clone[3] = NewImage.createRGBImage("temp" , width, height, 1, 1);

        ImagePlus result = NewImage.createRGBImage("temp" , 5*width, height, 1, 1); // final result
        ImageProcessor rip = result.getProcessor();
        ImagePlus bResult = NewImage.createRGBImage("temp" , 5*width, height, 1, 1); // final result
        ImageProcessor bRip = bResult.getProcessor();

        ImageProcessor[] ip = new ImageProcessor[3], bip = new ImageProcessor[3];

        rip = embed(rip, mip, 1); // embed the original photo in the first pos.

        for (int rgb = 0; rgb < 3; rgb++) {
            ip[rgb] = imageRGB[rgb].getProcessor();
            bip[rgb] = bImageRGB[rgb].getProcessor();
            ip[rgb] = calibrate(ip[rgb], bip[rgb]);
            rip = embed(rip, ip[rgb], rgb+2);
        }
        //public static ImagePlus mergeChannels(ImagePlus[] images,boolean keepSourceImages)
        combination = RGBStackMerge.mergeChannels(imageRGB, false);

        imc = new ImageConverter(combination);
        imc.convertToRGB();
        combIp = combination.getProcessor();
        rip = embed(rip, combIp, 5);
        bRip = embed(bRip, combIp, 1);

        for (int i = 0; i < 4; i++) {
            cip[i] = clone[i].getProcessor();
            cip[i] = copy(combIp, cip[i]); // copy the content of the original photo into each its clone
        }

        for (int binary = 0; binary < 4; binary++) {
            IJ.run(clone[binary], "Binary Layer "+binary, "");
            cip[binary] = clone[binary].getProcessor();
            bRip = embed(bRip, cip[binary], binary+2);

        }
        folderBuilder(path, target, photo);
        IJ.saveAs(combination, "jpeg", path+target+photo+photo+"-result"+postfix); // save as the result
        IJ.saveAs(result, "jpeg", path+target+photo+photo+"-all-"+postfix); // save as the result
        IJ.saveAs(bResult, "jpeg", path+target+photo+photo+"-binary-"+postfix); // save as the result
//        IJ.log("The result is saved in this following path succesfuly.");
//        IJ.log(path+target+photo);
        IJ.log("-------------------------------------------------------");
    }
}
