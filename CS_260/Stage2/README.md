﻿# Explanation regarding the procedure of the Filter:

# firstly, opens both the main image and its benchmark image, then, split those into their three channels, in case of RGB image, channels become R, G, B, and in case of HSB image, split channels become H, S, and V, then, the program does Calibration process in each channel separately, and eventually merge all of them into one image. in the end, do binary transformation and all procedures saved into three image files.
