import ij.IJ;
import ij.ImagePlus;
import ij.plugin.PlugIn;
import ij.process.ImageProcessor;
import ij.gui.NewImage;
import ij.plugin.ChannelSplitter;
import java.io.File;
import java.awt.Color;
import java.lang.Integer;

public class Helper2 implements PlugIn {
    public void run(String Args) {
        String path = "/media/projects/CS_260"; // common path, in my case is the absolute path of the project
        String ds = "/FEI _Face_DS/"; // path of the data set of images
        String target = "/Stage2/action1/original"; // the specific path of the task
        String code = "-11"; // the second part of name of the image file
        for (int iname = 173; iname < 178; iname++) { // loop over all data set images
            // the first part of name of the starting image file
            examine(path, ds, target, "/"+iname+code);
        }
    }
    private int[] getCumulativeHistogram(ImageProcessor ip) { // compute cumulative histogram
        int[] cumulativeHist = new int[256];
        int[] hist = ip.getHistogram();
        for (int i = 0; i < 256; i++) {
            for (int j = 0; j < i; j++) {
                cumulativeHist[i] += hist[j];
            }
        }
        return cumulativeHist;
    }
    private ImageProcessor sketch(int[] map, int width, int height, int colorCode) { // sketch diagram
        double max = 0.0, e = 0.0;
        int index = 0;
        ImagePlus Temp = NewImage.createRGBImage("temp" , width, height, 1, 1);
        ImageProcessor answer = Temp.getProcessor();
        for (int i: map) {
            if (i > max) {
                max = i;
            }
        }
        for (int u = 5; u < width-5; u++) {
            for (int v = 5; v < height-5; v++) {
                index = u*256;
                index /= (width-5);
                e = height-map[index]*height/max;
                if (v > e) {
                    answer.putPixel(u, v, colorCode); 
                } else {
                    answer.putPixel(u, v, 16777215); // Black
                }
            }
            colorCode += u/100;
        }
        return answer;
    }
    private ImageProcessor embed (ImageProcessor sup, ImageProcessor sub, int pos) {
        // embed given sub images into sup image with regard of given position
        int M = pos * sub.getWidth(), M0 = (pos - 1) * sub.getWidth();
        int N = sub.getHeight();
        for (int u = M0; u < M; u++) {
            for (int v = 0; v < N; v++) {
                sup.putPixel(u, v, sub.getPixel(u - M0, v));
            }
        }
        return sup;
    }
    private void folderBuilder(String path, String target, String photo) { // folder builder
        File file = new File(path+target+photo);
        boolean bool = file.mkdir();
        if (bool != false) IJ.log("photo folder is built.");
    }
    private void examine(String path, String ds, String target, String photo) {
        String postfix = ".jpg";
        IJ.open(path+ds+photo+postfix); // find the given photo name and open it
        ImagePlus image = IJ.getImage();
        ImagePlus[] imageRGB = ChannelSplitter.split(image);
        ImageProcessor mip = image.getProcessor();
        int width = mip.getWidth();
        int height =mip.getHeight();
        ImagePlus result = NewImage.createRGBImage("temp" , 4*width, height, 1, 1);
        ImageProcessor rip = result.getProcessor();
        ImageProcessor cip1, cip2;
        int[] hist = new int[256];
        int i = 2;
        int[] colors = {16723250, 1107250, 110};

        embed(rip, mip, 1);
        for (ImagePlus im: imageRGB) {
            cip1 = im.getProcessor();
            hist = getCumulativeHistogram(cip1);
            cip2 = sketch(hist, width, height, colors[i-2]);
            rip = embed(rip, cip2, i++);
        }
        folderBuilder(path, target, photo);
        IJ.saveAs(result, "jpeg", path+target+photo+photo+postfix); // save as the result
        IJ.log("The result is saved in this following path succesfuly.");
        IJ.log(path+target+photo);
        IJ.log("-------------------------------------------------------");
    }
}
 
