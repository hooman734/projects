import ij.IJ;
import ij.ImagePlus;
import ij.plugin.PlugIn;
import ij.process.ImageProcessor;
import ij.gui.NewImage;
import java.io.File;
import java.awt.Color;
import java.lang.String;

/**
 * Helper filter to do a Method  set by a range of parameters then do all binary filters and
 * stick all results together with original photo in a big, tabular format image as the result.
 */
public class Helper implements PlugIn {
  public void run(String Args) { // Main method

    String path = "/media/projects/CS_260"; // common path, in my case is the absolute path of the project
    String ds = "/FEI _Face_DS/"; // path of the data set of images
    String target = "/Stage1/action3"; // the specific path of the task
    String param = "/radius"; // the method's parameter
    String[] codes = {"-01", "-02", "-03", "-04", "-05", "-06", "-07", "-08", "-09", "-10", "-11", "-12", "-13", "-14"}; // the second part of name of the image file
    String[] Methods = {"/Median...", "/Mean...", "/Gaussian Blur...", "/Minimum...", "/Maximum...", "/Unsharp Mask...", "/Variance..."}; // some Filters
    int[] Radii = {1, 2, 3, 5, 8}; // based on the euiler’s number e =2.71 exponentiation from 0 and growth by 0.5

    for (String code: codes) {
      for (String method:Methods) { //loop over all filters
        for (int r:Radii) { // loop over all radii
          for (int iname = 173; iname < 178; iname++) { // loop over all data set images
            // the first part of name of the starting image file
            examine(path, ds, target, method, param, "/"+iname+code, r);
          }
        }
      }
    }

  }
  private ImageProcessor clone (ImageProcessor base, ImageProcessor clone) {
  // deep clone an image
    int M = base.getWidth();
    int N = base.getHeight();

    for (int u = 0; u < M; u++) {
      for (int v = 0; v < N; v++) {
        clone.putPixel(u, v, base.getPixel(u, v));
      }
    }
    return clone;
  }

  private ImageProcessor embed (ImageProcessor sup, ImageProcessor sub, int pos) {
  // embed given sub images into sup image with regard of given position
    int M = pos * sub.getWidth(), M0 = (pos - 1) * sub.getWidth();
    int N = sub.getHeight();
    for (int u = M0; u < M; u++) {
      for (int v = 0; v < N; v++) {
        sup.putPixel(u, v, sub.getPixel(u - M0, v));
      }
    }
    return sup;
  }

  private void examine(String path, String ds, String target, String method, String param, String photo, int r) {
    String postfix = ".jpg";
    IJ.open(path+ds+photo+postfix); // find the given photo name and open it
    ImagePlus image = IJ.getImage();
    ImageProcessor ip = image.getProcessor();
    ImagePlus [] clone = new ImagePlus[5]; // define four clones of the main photo
    ImageProcessor [] cip = new ImageProcessor[5];
    clone[0] = NewImage.createRGBImage("temp" , ip.getWidth(), ip.getHeight(), 1, 1);
    clone[1] = NewImage.createRGBImage("temp" , ip.getWidth(), ip.getHeight(), 1, 1);
    clone[2] = NewImage.createRGBImage("temp" , ip.getWidth(), ip.getHeight(), 1, 1);
    clone[3] = NewImage.createRGBImage("temp" , ip.getWidth(), ip.getHeight(), 1, 1);
    clone[4] = NewImage.createRGBImage("temp" , ip.getWidth(), ip.getHeight(), 1, 1);
    // create the result examination raw image with 6 times of original width and same height
    ImagePlus result = NewImage.createRGBImage("temp" , 6*ip.getWidth(), ip.getHeight(), 1, 1);
    ImageProcessor rip = result.getProcessor();
    rip = embed(rip, ip, 1); // embed original image to the first position
    IJ.run(image, method.substring(1), param.substring(1)+"="+r); // performing the input method
//    IJ.wait(5000); // wait for 3 SECs to let the process done
    for (int i = 0; i < 5; i++) {
      cip[i] = clone[i].getProcessor();
      cip[i] = clone(ip, cip[i]); // copy the content of the original photo into each its clone
    }
//    IJ.wait(3000); // wait for 3 SECs to let the process done
    for (int j = 0; j < 5; j++) {
      if (j > 0) {
        IJ.run(clone[j], "Binary Layer " + (j-1), ""); // do binary transform on clones
      }
      rip = embed(rip, cip[j], j+2); // embed all clones successivly into the result
    }
    folderBuilder(path, target, method, param, r, photo); // build proper folder for saving the result
    IJ.saveAs(result, "jpeg", path+target+method+param+r+photo+photo+postfix); // save as the result
    IJ.log("The result is saved in this following path succesfuly.");
    IJ.log(path+target+method+param+r+photo+photo+postfix);
    IJ.log("-------------------------------------------------------");
  }

  private void folderBuilder(String path, String target, String method, String param, int r, String photo) {
    File file1 = new File(path+target+method);
    boolean bool1 = file1.mkdir();
    File file2 = new File(path+target+method+param+r);
    boolean bool2 = file2.mkdir();
    File file3 = new File(path+target+method+param+r+photo);
    boolean bool3 = file3.mkdir();
    if (bool1 != false) IJ.log("folder is built.");
    if (bool2 != false) IJ.log("sub folder is built.");
    if (bool2 != false) IJ.log("photo folder is built.");
  }
}
