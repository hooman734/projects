﻿# Investigate the dependence of the extracted regions on Binary Layers on the head rotations and smile. If necessary, compute the Invariant Region Moments and Moment-based Geometric Properties of regions in the Binary Layers. Upload the results of in the Stage 1 sub-folder.

# Based on this experience I think per-processes like “Median”, “Mean” , and “Gaussian Blur” with radius range of 2 pixels to 5 pixels has the better result. Thus, that is my suggestion.
