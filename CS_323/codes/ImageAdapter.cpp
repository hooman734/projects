//
// Created by Hooman Hesamyan on 12/5/19.
//
#include "IMomentAdapter.cpp"
#include <cassert>

template <class T>
class ImageAdapter : public IMomentAdapter {
private:
    T * image = nullptr;
public:
    explicit ImageAdapter(T * image) {
        this->image = image;
        limit1 = image->getRangeX();
        limit2 = image->getRangeY();
    }
    int getLimit1() override {
        return limit1;
    }
    int getLimit2() override {
        return limit2;
    }
    double getValue(int x, int y) override {
        return image->getPixel(x, y);
    }
    void setLimit1(int limit1) override {
        image->setRangeX(limit1);
    }
    void setLimit2(int limit2) override {
        image->setRangeY(limit2);
    }
    void setValue(int x, int y, double v) override {
        image->setPixel(x, y, v);
    }
    T* getImage() {
        return image;
    }
};