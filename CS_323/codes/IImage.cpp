//
// Created by hooman on 12/8/19.
//
class IImage {
protected:
    double ** intensity = nullptr;
    int maxPixelX{1};
    int maxPixelY{1};
public:
    virtual double getPixel(int, int) = 0;
    virtual void setPixel(int u, int v, double intensity) =  0;
    int getRangeX() {
        return maxPixelX;
    }
    void setRangeX(int X) {
        maxPixelX = X;
    }
    int getRangeY() {
        return maxPixelY;
    }
    void setRangeY(int Y) {
        maxPixelY = Y;
    }
};