#include "Moment.cpp"
#include "ImageAdapter.cpp"
#include "SampleImage.cpp"

int main()
{
    int
        intensity0 = 0,
        intensity1 = 1,
        intensity2 = 2,
        intensity3 = 3,
        intensity4 = 4;

    SampleImage image(100, 100);                                 // sample image of size 10*10 with color's intensity among 0, 1, 2, 3, 4
    image.sketch();                                                   // draw it in command
    ImageAdapter<SampleImage> imageAd(&image);                        // adapt image sample to a plan distributed object proper to Moment class
    Moment<ImageAdapter<SampleImage>> obj(&imageAd);                  // build Moment object of the sample of Image
    std::cout << "Count = " << obj.getCount(intensity2) << std::endl; // write area of specific intensity
    std::cout << "Centroid = " << obj.getCentroid(intensity2)[0] << " , "
              << obj.getCentroid(intensity2)[1] << std::endl;               // write centroid of specific intensity
    std::cout << "Angle = " << obj.getOrientation(intensity2) << std::endl; // write angle of specific intensity
    std::cout << "Sigma = " << obj.getSigmaXY(intensity2)[0] << " , "
              << obj.getSigmaXY(intensity2)[1] << std::endl; // write sigma of specific intensity
    std::cout << "Central Box = " << obj.getCentralBoxDim(intensity2, 2)[0] << " , "
              << obj.getCentralBoxDim(intensity2, 2)[1] << std::endl;
    std::cout << "Eccentricity = " << obj.getEcc(intensity2) << std::endl;


    Moment<ImageAdapter<SampleImage>> obj2(obj.getCentralBox(intensity3));
    // sequence of the initial image to the newer one in smaller dimension
    std::cout << "Count = " << obj2.getCount(intensity2) << std::endl; // write area of specific intensity
    std::cout << "Centroid = " << obj2.getCentroid(intensity2)[0] << " , "
              << obj2.getCentroid(intensity2)[1] << std::endl;               // write centroid of specific intensity
    return 0;
}
