//
// Created by Hooman Hesamyan on 12/5/19.
//

class IMomentAdapter {
protected:
    int limit1{0}, limit2{0};

public:
    virtual int getLimit1() = 0;
    virtual int getLimit2() = 0;
    virtual double getValue(int, int) = 0;
    virtual void setLimit1(int) = 0;
    virtual void setLimit2(int) = 0;
    virtual void setValue(int, int, double) = 0;
};