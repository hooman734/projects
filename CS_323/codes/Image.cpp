//
// Created by hooman on 12/8/19.
//
#include <iostream>
#include <cassert>
#include "IImage.cpp"

class Image : public IImage {
public:
    Image(int u, int v) {
        setRangeX(u);
        setRangeY(v);
        intensity = new double * [maxPixelY];
        for (int i = 0; i < maxPixelY; ++i) {
            intensity[i] = new double [maxPixelX];
        }
        init();
    }
    void init() {
        for (int j = 0; j < maxPixelY; ++j) {
            for (int k = 0; k < maxPixelX; ++k) {
                intensity[j][k] = 4;
            }
        }
    }
    double getPixel(int u, int v) override {
        double answer = 0;
        assert (u <= maxPixelX && v <= maxPixelY);
        answer = intensity[v][u];
        return answer;
    }
    void setPixel(int u, int v, double intensity) override {
        assert (u <= maxPixelX && v <= maxPixelY);
        this->intensity[v][u] = intensity;
    }
};


