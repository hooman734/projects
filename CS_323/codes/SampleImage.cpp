#pragma clang diagnostic push
#pragma clang diagnostic push
#pragma ide diagnostic ignored "VirtualCallInCtorOrDtor"
#pragma ide diagnostic ignored "cert-msc30-c"
//
// Created by hooman on 12/8/19.
//
#include <iostream>
#include <cstdlib>
#include <ctime>
#include "Image.cpp"

class SampleImage : public Image {

public:
    SampleImage(int u, int v) : Image(u, v) {
        double i = 0;
        srand(time(nullptr));
        for (int j = 0; j < v; ++j) {
            for (int k = 0; k < u; ++k) {
                i = rand() % 5;
                setPixel(k, j, i);
            }
        }
    }
    void sketch() {
       for (int j = 0; j < getRangeY(); ++j) {
          for (int k = 0; k < getRangeX(); ++k) {
              std::cout << getPixel(k, j) << "  " ;
            }
            std::cout << std::endl;
        }
    }
    //void operator << (SampleImage const &obj) {}
};
#pragma clang diagnostic pop
