//
// Created by Hooman Hesamyan on 12/5/19.
//
#include <cmath>
#include <cassert>
#define PI 3.141592654
template <class T>
class Moment {

public:
    explicit Moment(T *obj) {
        this->planDistObj = obj;
        limit1 = planDistObj->getLimit1();
        limit2 = planDistObj->getLimit2();
        configuration();
    }
    double getCount(int value) {
        assert (value >= 0 && value <= 4);
        return count[value];
    }
    double getMoment(int p, int q, int value) {
        assert (value >= 0 && value <= 4);
        return calMoment(p, q, value);
    }
    double* getCentroid(int value) {
        assert (value >= 0 && value <= 4);
        return centroid[value];
    }
    double getOrientation(int value) {
        assert (value >= 0 && value <= 4);
        return orientation[value];
    }
    double* getSigmaXY(int value) {
        assert (value >= 0 && value <= 4);
        return sigmaXY[value];
    }
    double* getCentralBoxDim(int value, int corner) {
        assert (value >= 0 && value <= 4);
        assert (corner >= 0 && corner <=3);
        return centralBox[value][corner];
    }
    T * getCentralBox(int value) {
        T *planeDistObjShrink = nullptr;
        int boxWidth = 2 * ceil(sigmaXY[value][0]);
        int boxHeight = 2 * ceil(sigmaXY[value][1]);
        planeDistObjShrink->setLimit1(boxWidth);
        planeDistObjShrink->setLimit2(boxHeight);
        for (int i = 0; i < limit1; ++i) {
            for (int j = 0; j < limit2; ++j) {
                if (i > centralBox[value][0][0] && i < centralBox[value][1][0]) {
                    if (j > centralBox[value][0][1] && j < centralBox[value][3][1]) {
                        planeDistObjShrink->setValue(i-centralBox[value][0][0],
                                j-centralBox[value][0][1], planDistObj->getValue(i, j));
                    }
                }
            }
        }
        return planeDistObjShrink;
    }
    T * getBox() {
        return planDistObj;
    }
    double getEcc(int value) {
        assert (value >= 0 && value <= 4);
        return eccentricity[value];
    }
private:
    T * planDistObj;
    int limit1, limit2;
    double count[5]{};
    double m00[5]{}, m11[5]{}, m10[5]{}, m01[5]{}, m20[5]{}, m02[5]{}; // moments regarding with coordinate center
    double cm00[5]{}, cm11[5]{}, cm10[5]{}, cm01[5]{}, cm20[5]{}, cm02[5]{}; // moments regarding with center of image
    double centroid [5][2]{};
    double sigmaXY [5][2]{};
    double orientation[5]{};
    // the second index of 'centralBox' indicates the corner pose, top-left 0,
    // top-right 1, button-right 2, button-left 3
    double centralBox[5][4][2]{};
    double eccentricity[5]{};
    void configuration() {
        for (int i = 0; i < 5; ++i) {
            m00[i] = calMoment(0, 0, i);
            m11[i] = calMoment(1, 1, i);
            m10[i] = calMoment(1, 0, i);
            m01[i] = calMoment(0, 1, i);
            m20[i] = calMoment(2, 0, i);
            m02[i] = calMoment(0, 2, i);
            count[i] = m00[i] / i;
            calCentroid(i);
            cm00[i] = calCentralMoment(0, 0, i);
            cm11[i] = calCentralMoment(1, 1, i);
            cm10[i] = calCentralMoment(1, 0, i);
            cm01[i] = calCentralMoment(0, 1, i);
            cm20[i] = calCentralMoment(2, 0, i);
            cm02[i] = calCentralMoment(0, 2, i);
            calOrientation(i);
            calSD(i);
            calCentralBox(i);
            calEccentricity(i);
        }
    }
    double calMoment(int p, int q, int value) {
        double mpq = 0;
        for (int v = 0; v < limit2; ++v) {
            for (int u = 0; u < limit1; ++u) {
                if (value == planDistObj->getValue(u, v)) {
                    mpq += value * pow(u, p) * pow(v, q);
                }
            }
        }
        return mpq;
    }
    double calCentralMoment(int p, int q, int value) {
        double cmpq = 0;
        for (int v = 0; v < limit2; ++v) {
            for (int u = 0; u < limit1; ++u) {
                if (value == planDistObj->getValue(u, v)) {
                    cmpq += value * pow(u - centroid[value][0], p) * pow(v - centroid[value][1], q);
                }
            }
        }
        return cmpq;
    }
    void calSD(int value) {
        double sdx = 0, sdy = 0;
        for (int v = 0; v < limit2; ++v) {
            for (int u = 0; u < limit1; ++u) {
                if (value == planDistObj->getValue(u, v)) {
                    sdx += pow(u, 2);
                    sdy += pow(v, 2);
                }
            }
        }
        sdx = sdx / count[value] - pow(centroid[value][0], 2);
        sdy = sdy / count[value] - pow(centroid[value][1], 2);
        sigmaXY[value][0] = pow(sdx, 0.5);
        sigmaXY[value][1] = pow(sdy, 0.5);
    }
    void calCentroid(int value) {
        centroid[value][0] = m10[value] / m00[value];
        centroid[value][1] = m01[value] / m00[value];
    }
    void calOrientation(int value) {
        orientation[value] = (180/PI) * 0.5 * atan2(2 * cm11[value] , (cm20[value] - cm02[value]));
    }
    void calCentralBox(int value) {
        centralBox[value][0][0] = centroid[value][0] - sigmaXY[value][0] ;
        centralBox[value][0][1] = centroid[value][1] - sigmaXY[value][1] ;
        centralBox[value][1][0] = centroid[value][0] + sigmaXY[value][0];
        centralBox[value][1][1] = centroid[value][1] - sigmaXY[value][1];
        centralBox[value][2][0] = centroid[value][0] + sigmaXY[value][0];
        centralBox[value][2][1] = centroid[value][1] + sigmaXY[value][1];
        centralBox[value][3][0] = centroid[value][0] - sigmaXY[value][0];
        centralBox[value][3][1] = centroid[value][1] + sigmaXY[value][1];
    }
    void calEccentricity(int value) {
        double cmp = cm20[value] + cm02[value];
        double cmn = cm20 - cm02;
        double cmu = cm11[value];
        eccentricity[value] = (cmp + pow((cmn * cmn + 4 * cmu * cmu), 0.5))/
                (cmp - pow((cmn * cmn + 4 * cmu * cmu), 0.5));
    }
};
